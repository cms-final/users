package com.agh.cms.users.context;

public interface UserPublisher {

    void publishDeletion(String username);
}
