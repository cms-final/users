package com.agh.cms.users.context;

import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.infrastructure.exception.UnauthorizedException;
import com.agh.cms.users.infrastructure.CognitoRequestFactory;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
class UserService {

    private final AWSCognitoIdentityProvider awsCognitoIdentityProvider;
    private final UserPublisher userPublisher;
    private final UserBasicInfo userBasicInfo;

    private final ListGroupsRequest listGroupsRequest;
    private final ListUsersRequest listUsersRequest;
    private final AdminDeleteUserRequest adminDeleteUserRequest;

    UserService(AWSCognitoIdentityProvider awsCognitoIdentityProvider, UserPublisher userPublisher, UserBasicInfo userBasicInfo,
                CognitoRequestFactory cognitoRequestFactory) {
        this.awsCognitoIdentityProvider = awsCognitoIdentityProvider;
        this.userPublisher = userPublisher;
        this.userBasicInfo = userBasicInfo;
        listGroupsRequest = cognitoRequestFactory.listGroupsRequest();
        listUsersRequest = cognitoRequestFactory.listUsersRequest();
        adminDeleteUserRequest = cognitoRequestFactory.adminDeleteUserRequest();
    }

    Set<String> getAllUsernames() {
        return awsCognitoIdentityProvider.listUsers(listUsersRequest).getUsers().stream()
                .map(UserType::getUsername)
                .collect(Collectors.toSet());
    }

    Set<String> getAllGroupNames() {
        return awsCognitoIdentityProvider.listGroups(listGroupsRequest).getGroups().stream()
                .map(GroupType::getGroupName)
                .collect(Collectors.toSet());
    }

    void deleteUser(String username) {
        if (!userBasicInfo.group().equals("Admins")) {
            throw new UnauthorizedException();
        }
        adminDeleteUserRequest.setUsername(username);
        try {
            awsCognitoIdentityProvider.adminDeleteUser(adminDeleteUserRequest);
        } catch (UserNotFoundException | ResourceNotFoundException e) {
            throw new ResourceNotFoundException(username + "not found");
        }
        userPublisher.publishDeletion(username);
    }
}
