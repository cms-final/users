package com.agh.cms.users.infrastructure;

import com.amazonaws.services.cognitoidp.model.AdminDeleteUserRequest;
import com.amazonaws.services.cognitoidp.model.ListGroupsRequest;
import com.amazonaws.services.cognitoidp.model.ListUsersRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CognitoRequestFactory {

    private final String userPoolId;

    CognitoRequestFactory(@Value("${security.cognito.userPoolId}") String userPoolId) {
        this.userPoolId = userPoolId;
    }

    public ListUsersRequest listUsersRequest() {
        ListUsersRequest listUsersRequest = new ListUsersRequest();
        listUsersRequest.setUserPoolId(userPoolId);
        return listUsersRequest;
    }

    public ListGroupsRequest listGroupsRequest() {
        ListGroupsRequest listGroupsRequest = new ListGroupsRequest();
        listGroupsRequest.setUserPoolId(userPoolId);
        return listGroupsRequest;
    }

    public AdminDeleteUserRequest adminDeleteUserRequest() {
        AdminDeleteUserRequest adminDeleteUserRequest = new AdminDeleteUserRequest();
        adminDeleteUserRequest.setUserPoolId(userPoolId);
        return adminDeleteUserRequest;
    }
}
